---

name: "Bug Report"
about: "Report an issue!"
labels:
- bug

---


**Describe the issue:**


---


**Browser + Version:**

<!-- [X] means it is checked, [ ] means it is unchecked (Space inside brackets). -->

- [x] I have made sure this issue hasn't already been posted.
- [x] I have made sure I am on the latest version of my Browser.
- [x] I have made sure the platform I am running the style on is fully up-to-date.
- [X] I have made sure the platform I am running the style on does not have any heavy visual modifications

- [ ] Did I install as a Userstyle?
    - [ ] I am using the [Stylus](https://add0n.com/stylus.html) browser extension, not 'Stylish'.